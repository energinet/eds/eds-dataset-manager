[![pipeline status](https://gitlab.com/datopian/eds/api-poc/badges/master/pipeline.svg)](https://gitlab.com/datopian/eds/api-poc/commits/master)  [![coverage report](https://gitlab.com/datopian/eds/api-poc/badges/master/coverage.svg)](https://gitlab.com/datopian/eds/api-poc/commits/master)

API service for datasets management in a CKAN instance.

## Usage

Environment variables:

* `CKAN_API` - e.g., http://ckan:5000/api/3/action/ - must have a trailing slash.

### `/dataset_create`

* Method: POST
* Headers: `Authorization` that contains an api key for the CKAN instance
* Body:
  * `name` (required) - a name of the dataset.
    * This MUST be lower-case and contain only alphanumeric characters along with `.`, `_` or `-` characters.
    * This is also used as an alias for the table in the Datastore database.
  * `title` (optional) - a string providing a title or one sentence description for this dataset.
  * `description` (optional) - a description of the dataset.
    * This is also known as `notes` in the old metadata specification.
  * `comment` (optional) - a comment about the dataset.
  * `update_frequency` (required) - EDS specific metadata indicating how frequent a dataset is updated.
  * `resolution` (required) - EDS specific metadata indicating resolution of a dataset.
  * `owner_org` (required) - name of the organization to which this dataset belongs.
  * `metadata_language` (optional) - string indicating language of the metadata.
  * `author` (optional) - name of a dataset author (shown on package page as a text of a link).
  * `author_email` (optional) - email of a dataset author (shown on package page as `href` of a link).
  * `license_id` (optional) - preferably human-readable id of a license.
  * `license_url` (optional) - a URL for a license.
  * `tags` (optional) - list of dictionaries with tags, e.g., `[{"name": "tag-name"}]`
    * If the tag doesn't exist, it will crate a new one.
  * `groups` (optional) - list of dictionaries, see docs about creating groups for attributes.
  * `resources` - a list (array) of resources in this dataset. Each item in this list must be an object (dictionary) with the following properties:
    * `name` (required) - a name of a resource.
      * This MUST be lower-case and contain only alphanumeric characters along with `.`, `_` or `-` characters.
    * `title` (optional) - a title or label for the resource.
    * `format` (required) - 'csv', 'xls', 'json' etc. Would be expected to be the standard file extension for this type of resource.
      * In EDS environment, we have single format which is `Data`.
    * `description` (optional) - a description of the resource.
    * `schema`
      * `fields` (required) - list of dictionaries describing fields (columns) of the data:
        * `name` (required) - name of field (e.g. column name).
        * `type` (required) - a string specifying the type.
        * `title` (optional) - a nicer human readable label or title for the field. If not provided, it is equal to `name`.
        * `description` (optional) - a description for the field.
        * `comment` (optional) - a comment for the field.
        * `size` (optional) - a size used when displaying data for the user.
          * For strings and timestamps, it is an integer that tells how many positions should be displayed.
          * For numbers, it also indicates number of decimal places.
        * `format_regex` (optional)
        * `property_constraint` (optional)
        * `validation_rules` (optional) - string describing validation rules for the field.
        * `example` (optional)
        * `unit` (optional) - string indicating unit.
      * `primary_key` (required) - list of field (column) names that should be set as primary keys in the datastore database.

Example with required properties only:

```json
{
  "name": "test",
  "resources": [
    {
      "name": "test",
      "schema": {
        "fields": [
          {
            "name": "column",
            "type": "string",
            "format": "Data"
          }
        ],
        "primary_key": ["column"]
      }
    }
  ],
  "owner_org": "test_org",
  "resolution": "One hour (PT1H)",
  "update_frequency": "P1D"
}
```

Response:

```json
{
  "success": true,
  "message": "Dataset is created"
}
```

Check out this diagram to oversee what's happening:

```mermaid
sequenceDiagram
  User->>NewAPI: create a dataset - datapackage.json
  NewAPI-->>NewAPI: Validate provided datapackage.json
  NewAPI-->>NewAPI: Prepare CKAN-like package descriptor
  NewAPI->>CKAN: Call "package_create"
  CKAN-->>CKAN: Creates a package
  CKAN-->>NewAPI: Done!
  NewAPI-->>NewAPI: Prepare body for Datastore
  NewAPI-->>NewAPI: Convert tableschema field types into Datastore format
  NewAPI->>Datastore: Call "custom_datastore_create"
  Datastore-->>CKAN: Creates a resource
  Datastore-->>Datastore: Creates a table for this resource
  Datastore-->>Datastore: Creates an alias for this resource
  Datastore-->>NewAPI: Done!
  NewAPI->>User: Done!
```

### `/dataset_purge`

Purge a dataset with single API call. It will delete:

* A package from the CKAN database
* A resource from  the CKAN database
* A table from from the Datastore database (including all associated aliases)

How to use it:

* Method: POST
* Headers: `Authorization` that contains an api key for the CKAN instance
* Body: JSON with the name of dataset to be purged

Example body:

```json
{
  "name": "test"
}
```

Response:

```json
{
  "success": true,
  "message": "Dataset is purged"
}
```

Check out this diagram to oversee what's happening:

```mermaid
sequenceDiagram
  User->>NewAPI: delete a dataset - {"name": "my-dataset"}
  NewAPI-->>CKAN: Get resources for this dataset
  CKAN-->>NewAPI: Done! Here is the list of resources
  NewAPI-->>Datastore: Delete a table for a resource
  Datastore-->>Datastore: Delete a table and associated aliases
  Datastore-->>NewAPI: Done!
  NewAPI-->>CKAN: Purge this dataset
  CKAN-->>CKAN: Delete resources, the package, associated revisions etc. from CKAN DB
  CKAN-->>NewAPI: Done!
  NewAPI->>User: Done!
```

### `/dataset_update`

Update metadata of a dataset including package and resource level metadata. Note that only explicitly defined properties will be updated.

How to use it:

* Method: POST
* Headers: `Authorization` that contains an api key for the CKAN instance
* Body: either a valid or shallow data package descriptor (`name` property is required to identify which dataset to update)

Example body (consider as an update for descriptor we used in `dataset_create` example):

```json
{
  "name": "test",
  "title": "Title of my dataset",
  "resources": [
    {
      "name": "test",
      "title": "Title of my resource"
    }
  ]
}
```

This will add `title` properties for the package and resource. It will not change properties such as `resolution` etc.

Check out this diagram to oversee what's happening:

```mermaid
sequenceDiagram
    User->>NewAPI: Update my dataset - here is the new descriptor
    NewAPI->>CKAN: Hey, give me current descriptor for the dataset
    CKAN-->>NewAPI: Done - `ckanPackage`
    NewAPI-->>NewAPI: Convert `ckanPackage` to Data Package
    NewAPI-->>NewAPI: Merge new and current descriptors so that new one overrides old one
    NewAPI->>CKAN: Patch this dataset (not providing resource descriptor yet)
    CKAN-->>NewAPI: Done!
    NewAPI-->>NewAPI: Merge new and current resource descriptor
    NewAPI->>CKAN: Patch this resource
    CKAN-->>NewAPI: Done!
    NewAPI->>User: Your dataset is updated!
```

### `/group/:action`

Available actions:

* `show`
* `create`
* `patch`
* `purge`

#### Show group

Usage:

* path: `/group/show/name`
* method: `GET`

Example:

`/group/show/my-group-name`

#### Create group

Usage:

* path: `/group/create`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - the name of the group, a string between 2 and 100 characters long, containing only lowercase alphanumeric characters, `-` and `_`
  * `title` - the title of the group (optional)
  * `description` - the description of the group (optional)
  * `image_url` - the URL to an image to be displayed on the group’s page (optional)
  * `packages` (list of dictionaries) - the datasets (packages) that belong to the group, a list of dictionaries each with keys `'id'` (string, the name of the dataset) and optionally `'title'` (string, the title of the dataset)

Example of body for this request:

```
{
  "name": "my-group-name",
  "title": "My human readable group name",
  "description": "This is an example group.",
  "image_url": "https://example.com/picture.png",
  "packages": [{"id": "my-dataset"}]
}
```

#### Patch group

Usage:

* path: `/group/patch`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - name of the group
  * for other parameters, please, see group create action

Example of body for this request:

```
{
  "name": "my-group-name",
  "title": "Updated title"
}
```

#### Purge group

Usage:

* path: `/group/purge`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - name of the group

Example of body for this request:

```
{
  "name": "my-group-name"
}
```

### `/organization/:action`

Available actions:

* `show`
* `create`
* `patch`
* `purge`

#### Show organization

Usage:

* path: `/organization/show/name`
* method: `GET`

Example:

`/organization/show/my-organization-name`

#### Create organization

Usage:

* path: `/organization/create`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - the name of the organization, a string between 2 and 100 characters long, containing only lowercase alphanumeric characters, `-` and `_`
  * `title` - the title of the organization (optional)
  * `description` - the description of the organization (optional)
  * `image_url` - the URL to an image to be displayed on the organization's page (optional)
  * `packages` (list of dictionaries) - the datasets (packages) that belong to the organization, a list of dictionaries each with keys `'name'` (string, the name of the dataset) and optionally `'title'` (string, the title of the dataset)

Example of body for this request:

```
{
  "name": "my-organization-name",
  "title": "My human readable organization name",
  "description": "This is an example organization.",
  "image_url": "https://example.com/picture.png",
  "packages": [{"name": "my-dataset"}]
}
```

#### Patch organization

Usage:

* path: `/organization/patch`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - name of the organization
  * for other parameters, please, see organization create action

Example of body for this request:

```
{
  "name": "my-organization-name",
  "title": "Updated title"
}
```

#### Purge organization

Usage:

* path: `/organization/purge`
* method: `POST`
* header: `Authorization`
  * this is API key which is required to perform the request
* body
  * `name` - name of the organization

Example of body for this request:

```
{
  "name": "my-organization-name"
}
```

### Helpers

#### `/validate`

Validate data packages:

* Method: POST
* Body: JSON object to validate

Response:

```json
{
  "valid": true,
  "message": "Your data package descriptor is valid"
}
```

#### `/ckan-to-data-package`

Convert CKAN like descriptor into data package:

* Method: POST
* Body: JSON object to convert (CKAN like descriptor)

Response:

Converted data package descriptor.

## Repair datasets

In some situations, we can have errors when managing a dataset. Below are known cases and suggested solutions.

### A dataset is already in CKAN DB

When you call "dataset_create" API, you might receive following error:

```json
{
  "help": "https://edstest01.energidataservice.dk/api/3/action/help_show?name=package_create",
  "success":false,
  "error": {
    "__type": "Validation Error",
    "id": ["Dataset id already exists"]
  }
}
```

Reason:

This error message means that the `packages` table in the CKAN database already has the id. This can happen if previous `dataset_create` API has failed or not completed due to errors in Datastore.

Possible solutions:

* Option 1: you can call `/dataset_purge` API of new dataset manager service. Body of the request would be: `{"name": "name-of-your-dataset"}`
* Option 2: you can call our API wrapper in the extension. E.g., for test env: http://edstest01.energidataservice.dk/api/3/action/custom_dataset_purge with body `{"id": "name-of-your-dataset"}`
* Option 3: you can call CKAN API directly. E.g., for test env: edstest01.energidataservice.dk/api/3/action/dataset_purge with body `{"id": "name-of-your-dataset"}`

### A dataset is already in Datastore or only alias is left in the DB

Error message is unknown at the moment but we will paste it here once we experience it.

Reason:

This happens due to existence of an alias in the Datastore. To confirm it, you can check list of tables and their aliases from Datastore by going to http://edstest01.energidataservice.dk/api/3/action/datastore_search?resource_id=_table_metadata

Possible solutions:

* Option 1: you can call our API wrapper in the extension `datastore_junk_delete`. E.g., for test env: http://edstest01.energidataservice.dk/api/3/action/datastore_junk_delete with body `{"resource_id": "your-resource-id"}`
  * How would you know your resource id? Check http://edstest01.energidataservice.dk/api/3/action/datastore_search?resource_id=_table_metadata and find your alias which will give you the id of a resource.

### Field type difference between tableschema and datastore

Below is the list of field types demonstrating difference in type names:

```
[
  {
    eds: 'text',
    datastore: 'text',
    tableschema: 'string'
  },
  {
    eds: 'int',
    datastore: 'int',
    tableschema: 'integer'
  },
  {
    eds: 'float',
    datastore: 'float',
    tableschema: 'number'
  },
  {
    eds: 'date',
    datastore: 'date',
    tableschema: 'date'
  },
  {
    eds: 'time',
    datastore: 'time',
    tableschema: 'time'
  },
  {
    eds: 'timestamp',
    datastore: 'timestamp',
    tableschema: 'datetime'
  },
  {
    eds: 'timestamptz',
    datastore: 'timestamp',
    tableschema: 'datetime'
  },
  {
    eds: 'bool',
    datastore: 'bool',
    tableschema: 'boolean'
  },
  {
    eds: 'json',
    datastore: 'json',
    tableschema: 'object'
  }
]
```

## Test

First, install dev dependancies:

```bash
yarn install
# or with npm
npm install
```

Run once with coverage report:

```bash
npx ava
# or with yarn:
yarn test
# or with npm script:
npm test
```

Watch mode:

```bash
npx ava --watch
# or with yarn:
yarn test --watch
```

Verbose mode:

```bash
npx ava --verbose
# or with yarn:
yarn test --verbose
```

const url = require('url')

const express = require('express')
const fetch = require('node-fetch')
const assignDeep = require('assign-deep')
const {
  validateMetadata,
  ckanPackageToDataPackage,
  preparePackageCreateBody,
  prepareDatastoreBody,
  preparePackagePatchBody,
  prepareResourcePatchBody
} = require('../lib')


function ckanApi(action, method, body, apiKey) {
  let options = {
    method,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': apiKey
    }
  }
  if (method === 'POST') {
    options.body = JSON.stringify(body)
  }

  return fetch(url.resolve(process.env.CKAN_API, action), options)
}


module.exports = function () {
  const router = express.Router()

  router.get('/status', (req, res) => {
    res.send('server is running')
  })

  router.post('/dataset_create', async (req, res, next) => {
    // Take an api key from the header and use it as api key for CKAN
    const apiKey = req.get('Authorization')
    let descriptor = req.body
    // Validate given data package descriptor
    const validation = await validateMetadata(descriptor)
    if (!validation.valid) {
      res.setHeader('Content-Type', 'text/plain')
      res.status(400)
      res.end(JSON.stringify({
        message: validation.error
      }, null, 2))
      return
    }

    try {
      // Prepare body for 'package_create' API
      const pkgBody = preparePackageCreateBody(descriptor)
      let pkgCreateRes = await ckanApi('package_create', 'POST', pkgBody, apiKey)
      if (!pkgCreateRes.ok) {
        let message = ''
        if (pkgCreateRes.headers.get('Content-Type') === 'application/json') {
          message = JSON.stringify(await pkgCreateRes.json())
          res.setHeader('Content-Type', 'application/json')
        } else {
          message = await pkgCreateRes.text()
          res.setHeader('Content-Type', 'text/plain')
        }
        res.status(pkgCreateRes.status)
        res.end(message)
        return
      }
      // Prepare body for custom_datastore_create API
      const datastoreBody = prepareDatastoreBody(descriptor)
      let datastoreCreateRes = await ckanApi('custom_datastore_create', 'POST', datastoreBody, apiKey)
      if (!datastoreCreateRes.ok) {
        // Perform DB clean up, eg, remove created package:
        // Call 'dataset_purge' action from classic CKAN. Note that it is not
        // same as 'dataset_manager' app's `dataset_purge` API:
        const ckanPkgPurgeUrl = url.resolve(process.env.CKAN_API, 'dataset_purge')
        const ckanPkgPurgeRes = await ckanApi('dataset_purge', 'POST', {id: pkgBody.name}, apiKey)
        if (!ckanPkgPurgeRes.ok) {
          res.setHeader('Content-Type', 'text/plain')
          res.status(ckanPkgPurgeRes.status)
          res.end('Error happened during DB cleanup: the package was created but failed to create a table in the Datastore.')
          return
        }
        // Now response with error message to the client:
        let message = ''
        if (datastoreCreateRes.headers.get('Content-Type') === 'application/json') {
          message = JSON.stringify(await datastoreCreateRes.json())
          res.setHeader('Content-Type', 'application/json')
        } else {
          message = await datastoreCreateRes.text()
          res.setHeader('Content-Type', 'text/plain')
        }
        res.status(datastoreCreateRes.status)
        res.end(message)
        return
      }
      // Respond
      const response = {
        success: true,
        message: 'Dataset is created'
      }
      res.setHeader('Content-Type', 'application/json')
      res.status(200)
      res.end(JSON.stringify(response))
    } catch (e) {
      next(e)
    }
  })


  router.post('/dataset_purge', async (req, res, next) => {
    try {
      // Take an api key from the header and use it as api key for CKAN
      const apiKey = req.get('Authorization')
      // Call custom_dataset_purge action
      const body = {id: req.body.name || req.body.id}
      const datasetPurgeRes = await ckanApi('custom_dataset_purge', 'POST', body, apiKey)
      const datasetPurgeResJson = await datasetPurgeRes.json()
      if (!datasetPurgeRes.ok) {
        res.setHeader('Content-Type', 'application/json')
        res.status(datasetPurgeRes.status)
        res.end(JSON.stringify(datasetPurgeResJson))
        return
      }
      // Respond
      const response = {
        success: true,
        message: 'Dataset is purged'
      }
      res.setHeader('Content-Type', 'application/json')
      res.status(200)
      res.end(JSON.stringify(response))
    } catch (e) {
      next(e)
    }
  })


  router.post('/dataset_update', async (req, res, next) => {
    try {
      // Take an api key from the header and use it as api key for CKAN
      const apiKey = req.get('Authorization')
      let descriptor = req.body
      // Get CKAN package descriptor via 'package_show' API
      const packageShowRes = await ckanApi(`package_show?name_or_id=${descriptor.name}`, 'GET', null, apiKey)
      const packageShowResJson = await packageShowRes.json()
      if (!packageShowRes.ok) {
        res.setHeader('Content-Type', 'application/json')
        res.status(packageShowRes.status)
        res.end(JSON.stringify(packageShowResJson))
        return
      }
      const ckanPackage = packageShowResJson.result
      // Convert to data package descriptor
      const currentDataPackage = ckanPackageToDataPackage(ckanPackage)
      // Take resources for later use
      const currentResourceDescriptor = currentDataPackage.resources ? currentDataPackage.resources[0] : {}
      const newResourceDescriptor = descriptor.resources ? descriptor.resources[0] : {}
      delete currentDataPackage.resources
      delete descriptor.resources
      // Merge provided data package into current one
      const updatedDataPackage = assignDeep(currentDataPackage, descriptor)
      // Prepare body for package_patch API
      const packagePatchBody = preparePackagePatchBody(updatedDataPackage)
      let packagePatchRes = await ckanApi('package_patch', 'POST', packagePatchBody, apiKey)
      const packagePatchResJson = await packagePatchRes.json()
      if (!packagePatchRes.ok) {
        res.setHeader('Content-Type', 'application/json')
        res.status(packagePatchRes.status)
        res.end(JSON.stringify(packagePatchResJson))
        return
      }
      // Next update resource descriptor
      // Merge new descriptor into current one
      // But first make sure 'type' and 'name' properties of each field are unchanged
      let invalidMetadataMessage
      if (newResourceDescriptor.schema && newResourceDescriptor.schema.fields) {
        newResourceDescriptor.schema.fields.some((field, index) => {
          if (!field.name || !field.type) {
            invalidMetadataMessage = JSON.stringify(
              {
                success: false,
                error: {
                  message: 'Explicitly provide `name` and `type` properties of a field.'
                }
              }
            )
            return true
          }
          if (field.name !== currentResourceDescriptor.schema.fields[index].name || field.type !== currentResourceDescriptor.schema.fields[index].type) {
            invalidMetadataMessage = JSON.stringify(
              {
                success: false,
                error: {
                  message: 'Modifying `name` and `type` properties of field is forbidden.'
                }
              }
            )
            return true
          }
        })
      }
      if (invalidMetadataMessage) {
        res.setHeader('Content-Type', 'application/json')
        res.status(400)
        res.end(invalidMetadataMessage)
        return
      }
      // Remove empties
      newResourceDescriptor.schema.fields = newResourceDescriptor.schema.fields.filter(field => Object.keys(field).length > 0)
      const mergedResourceDescriptor = assignDeep(currentResourceDescriptor, newResourceDescriptor)
      const resourcePatchUrl = url.resolve(process.env.CKAN_API, 'resource_patch')
      // Prepare body for resource_patch API
      const resourcePatchBody = prepareResourcePatchBody(mergedResourceDescriptor)
      let resourcePatchRes = await ckanApi('resource_patch', 'POST', resourcePatchBody, apiKey)
      const resourcePatchResJson = await resourcePatchRes.json()
      if (!resourcePatchRes.ok) {
        res.setHeader('Content-Type', 'application/json')
        res.status(resourcePatchRes.status)
        res.end(JSON.stringify(resourcePatchResJson))
        return
      }
      // Respond success message
      const response = {
        success: true,
        message: 'Dataset is updated'
      }
      res.setHeader('Content-Type', 'application/json')
      res.status(200)
      res.end(JSON.stringify(response))
    } catch (e) {
      next(e)
    }
  })


  // Groups and organizations

  router.get('/:type(group|organization)/show/:name', async (req, res, next) => {
    const entity = req.params.type
    const name = req.params.name
    const ckanAction = entity + '_show?id=' + name + '&include_datasets=True&include_dataset_count=False&include_extras=False&include_users=False&include_groups=False'
    const ckanResponse = await ckanApi(ckanAction, req.method)
    const ckanResponseJson = await ckanResponse.json()
    res.setHeader('Content-Type', 'application/json')
    res.status(ckanResponse.status)
    if (!ckanResponse.ok) {
      res.end(JSON.stringify(ckanResponseJson.error))
      return
    }
    // Return only needed keys from full CKAN response:
    const {
      approval_status,
      image_display_url,
      is_organization,
      type,
      id,
      num_followers,
      revision_id,
      state,
      created,
      ...response
    } = ckanResponseJson.result
    res.end(JSON.stringify(response))
    return
  })

  router.post('/:type(group|organization)/:action', async (req, res, next) => {
    const entity = req.params.type
    const actions = ['create', 'patch', 'purge']
    if (!actions.includes(req.params.action)) {
      res.setHeader('Content-Type', 'application/json')
      res.status(400)
      res.end(JSON.stringify({success: false, message: "Unknown action"}))
      return
    }
    const body = JSON.parse(JSON.stringify(req.body))
    body.id = body.name
    const apiKey = req.get('Authorization')
    const ckanAction = entity + '_' + req.params.action
    const ckanResponse = await ckanApi(ckanAction, req.method, body, apiKey)
    if (!ckanResponse.ok) {
      const ckanResponseJson = await ckanResponse.json()
      res.setHeader('Content-Type', 'application/json')
      res.status(ckanResponse.status)
      res.end(JSON.stringify(ckanResponseJson))
      return
    }
    // Respond success message
    const response = {
      success: true,
      message: `${entity} ${req.params.action} succeeded for "${req.body.name}".`
    }
    res.setHeader('Content-Type', 'application/json')
    res.status(200)
    res.end(JSON.stringify(response))
  })

  router.post('/validate', async (req, res, next) => {
    try {
      const descriptor = req.body
      const validation = await validateMetadata(descriptor)
      const response = {
        valid: true,
        message: 'Your data package descriptor is valid'
      }
      if (!validation.valid) {
        response.valid = false
        response.message = validation.error
      }
      res.setHeader('Content-Type', 'application/json')
      res.status(200)
      res.end(JSON.stringify(response, null, 2))
    } catch (e) {
      next(e)
    }
  })


  router.post('/ckan-to-data-package', (req, res, next) => {
    try {
      const ckanDescriptor = req.body
      const dpDescriptor = ckanPackageToDataPackage(ckanDescriptor)
      res.setHeader('Content-Type', 'application/json')
      res.status(200)
      res.end(JSON.stringify(dpDescriptor, null, 2))
    } catch (e) {
      next(e)
    }
  })


  return router
}

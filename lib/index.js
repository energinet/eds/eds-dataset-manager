const assignDeep = require('assign-deep')
const { Validator } = require('datahub-client')


const FIELDS = [
  {
    eds: 'text',
    datastore: 'text',
    tableschema: 'string'
  },
  {
    eds: 'int',
    datastore: 'int',
    tableschema: 'integer'
  },
  {
    eds: 'float',
    datastore: 'float',
    tableschema: 'number'
  },
  {
    eds: 'date',
    datastore: 'date',
    tableschema: 'date'
  },
  {
    eds: 'time',
    datastore: 'time',
    tableschema: 'time'
  },
  {
    eds: 'timestamp',
    datastore: 'timestamp',
    tableschema: 'datetime'
  },
  {
    eds: 'timestamptz',
    datastore: 'timestamp',
    tableschema: 'datetime'
  },
  {
    eds: 'bool',
    datastore: 'bool',
    tableschema: 'boolean'
  },
  {
    eds: 'json',
    datastore: 'json',
    tableschema: 'object'
  }
]

function ckanPackageToDataPackage(descriptor) {
  // TODO: disabling most of conversions as need to switch off validator from core CKAN
  // Make a copy
  const datapackage = JSON.parse(JSON.stringify(descriptor))

  // Lowercase name
  datapackage.name = datapackage.name.toLowerCase()

  // Rename notes => description
  if (datapackage.notes) {
    datapackage.description = datapackage.notes
    delete datapackage.notes
  }

  // Rename ckan_url => homepage
  // if (datapackage.ckan_url) {
  //   datapackage.homepage = datapackage.ckan_url
  //   delete datapackage.ckan_url
  // }

  // Parse license
  // const license = {}
  // if (datapackage.license_id) {
  //   license.type = datapackage.license_id
  //   delete datapackage.license_id
  // }
  // if (datapackage.license_title) {
  //   license.title = datapackage.license_title
  //   delete datapackage.license_title
  // }
  // if (datapackage.license_url) {
  //   license.url = datapackage.license_url
  //   delete datapackage.license_url
  // }
  // if (Object.keys(license).length > 0) {
  //   datapackage.license = license
  // }

  // Parse author and sources
  // const source = {}
  // if (datapackage.author) {
  //   source.name = datapackage.author
  //   delete datapackage.author
  // }
  // if (datapackage.author_email) {
  //   source.email = datapackage.author_email
  //   delete datapackage.author_email
  // }
  // if (datapackage.url) {
  //   source.web = datapackage.url
  //   delete datapackage.url
  // }
  // if (Object.keys(source).length > 0) {
  //   datapackage.sources = [source]
  // }

  // Parse maintainer
  // const author = {}
  // if (datapackage.maintainer) {
  //   author.name = datapackage.maintainer
  //   delete datapackage.maintainer
  // }
  // if (datapackage.maintainer_email) {
  //   author.email = datapackage.maintainer_email
  //   delete datapackage.maintainer_email
  // }
  // if (Object.keys(author).length > 0) {
  //   datapackage.author = author
  // }

  // Parse tags
  // if (datapackage.tags) {
  //   datapackage.keywords = []
  //   datapackage.tags.forEach(tag => {
  //     datapackage.keywords.push(tag.name)
  //   })
  //   delete datapackage.tags
  // }

  // Parse extras
  // TODO

  // Resources
  if (datapackage.resources) {
    datapackage.resources = datapackage.resources.map(resource => {
      if (resource.name) {
        resource.title = resource.title || resource.name
        resource.name = resource.name.toLowerCase().replace(/ /g, '_')
      } else {
        // EDS specific:
        resource.name = resource.resource ? resource.resource.name : datapackage.name
      }

      if (resource.url) {
        resource.path = resource.url
        delete resource.url
      }

      // Handle tableschema
      if (!resource.schema) {
        // Note that in old EDS system, it has `fields` property with similar data.
        const resourceFields = resource.attributes || resource.fields || []
        resource.schema = {
          fields: resourceFields.map(field => {
            return {
              name: field.name_of_field,
              title: field.name_of_attribute,
              description: field.attribute_description,
              comment: field.comment,
              example: field.example,
              unit: field.unit,
              type: FIELDS.find(item => item.eds === field.type).tableschema,
              format: 'any',
              constraints: {
                pattern: field.format_regex
                // TODO: handle size attribute
              }
            }
          })
        }
      }

      delete resource.fields
      delete resource.attributes
      delete resource.resource
      return resource
    })
  }

  return datapackage
}


async function validateMetadata(descriptor) {
  // Add fake 'path' property to pass validation
  if (descriptor.resources && descriptor.resources[0] && !descriptor.resources[0].path) {
    descriptor.resources[0].path = 'fake'
  }
  const validator = new Validator()
  try {
    await validator.validateMetadata(descriptor)
    return {
      valid: true
    }
  } catch(e) {
    let error = e.map(message => message.toString().replace(/\n/g, ''))
    return {
      valid: false,
      error
    }
  }
}


function preparePackageCreateBody(descriptor) {
  const defaults = {
    private: false,
    author: 'Energinet',
    author_email: 'energidata@energinet.dk'
  }
  const newDescriptor = JSON.parse(JSON.stringify(descriptor))
  newDescriptor.notes = descriptor.description || newDescriptor.notes
  delete newDescriptor.description
  delete newDescriptor.resources
  // Copy values of newDescriptor into defaults so that we have no missing values:
  return Object.assign(defaults, newDescriptor)
}


function prepareDatastoreBody(descriptor) {
  const copyOfDescriptor = JSON.parse(JSON.stringify(descriptor))
  const body = {
    resource: {
      package_id: copyOfDescriptor.name
    },
    fields: JSON.parse(JSON.stringify(copyOfDescriptor.resources[0].schema.fields)),
    aliases: copyOfDescriptor.name,
    primary_key: copyOfDescriptor.resources[0].schema.primaryKey || copyOfDescriptor.resources[0].schema.primary_key
  }
  // Convert field descriptor to datastore format
  body.fields = body.fields.map(field => {
    field.id = field.name
    const fieldDict = FIELDS.find(item => ((item.tableschema === field.type) || (item.datastore === field.type)))
    if (fieldDict) {
      field.type = fieldDict.datastore
    }
    if (field.description) {
      field.info = {notes: field.description}
    }
    return field
  })
  body.resource = assignDeep(body.resource, copyOfDescriptor.resources[0])
  return body
}


function preparePackagePatchBody(descriptor) {
  // Make sure 'notes' are updated using 'description' property
  descriptor.notes = descriptor.description || descriptor.notes
  // Make sure 'url' isn't null
  if (descriptor.url === null) {
    descriptor.url = ''
  }

  return descriptor
}


function prepareResourcePatchBody(resource) {
  const defaults = {
    url: '',
    url_type: 'datastore'
  }
  return assignDeep(defaults, resource)
}


module.exports = {
  validateMetadata,
  ckanPackageToDataPackage,
  preparePackageCreateBody,
  prepareDatastoreBody,
  preparePackagePatchBody,
  prepareResourcePatchBody
}

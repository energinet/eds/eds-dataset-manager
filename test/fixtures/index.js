const { mocksForDatasetActions } = require('./datasets')
const { mocksForOrgsAndGroupsActions } = require('./orgs-and-groups')


module.exports.initMocks = function() {
  mocksForDatasetActions()
  mocksForOrgsAndGroupsActions()
}

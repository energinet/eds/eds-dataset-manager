const nock = require('nock')
const utils = require('../../lib')

module.exports.mocksForDatasetActions = function () {
  const descriptor = {
    "name": "capacityauctiondk-test",
    "description": "PTR is explicit auction of transfer capacity on a connection between two price areas",
    "title": "Auction of Capacities, PTR DK1-Germany",
    "resolution": "One hour (PT1H)",
    "update_frequency": "P1D",
    "owner_org": "test_org_00",
    "private": false,
    "resources": [
      {
        "name": "capacityauctiondk-test",
        "path": "fake",
        "schema": {
          "fields": [
            {
              "name": "a",
              "type": "string"
            },
            {
              "name": "b",
              "type": "string"
            }
          ],
          "primaryKey": ["a"]
        }
      }
    ]
  }

  const pkgBody = utils.preparePackageCreateBody(descriptor)

  const datastoreBody = utils.prepareDatastoreBody(descriptor)

  // Datastore create fails if field name has a space as per Postgresql naming
  // convention. We want to test that case:
  const copyOfDescriptorForDatastore = JSON.parse(JSON.stringify(descriptor))
  copyOfDescriptorForDatastore.resources[0].schema.fields[0].name += ' '
  const datastoreBodyError = utils.prepareDatastoreBody(copyOfDescriptorForDatastore)

  const copyOfDescriptor = JSON.parse(JSON.stringify(descriptor))
  const resources = copyOfDescriptor.resources
  delete copyOfDescriptor.resources
  let pkgPatchBody = utils.preparePackagePatchBody(copyOfDescriptor)
  pkgPatchBody.title = 'New title'

  const resourcePatchBody = utils.prepareResourcePatchBody(resources[0])
  resourcePatchBody.title = 'New res title'

  nock('http://ckan:5000/api/3/action/', {reqheaders: {authorization: 'test-api-key'}})
    .persist()
    .post('/package_create', pkgBody)
    .reply(200, {success: true})
    .post('/custom_datastore_create', datastoreBody)
    .reply(200, {success: true})
    .post('/custom_datastore_create', datastoreBodyError)
    .reply(500, {success: false, error: {fields: '"a " is not a valid field name'}})
    .post('/dataset_purge', {id: 'capacityauctiondk-test'})
    .reply(200, {success: true})
    .post('/custom_dataset_purge', {id: 'capacityauctiondk-test'})
    .reply(200, {success: true, message: 'Dataset is purged'})
    .post('/package_patch', pkgPatchBody)
    .reply(200, {success: true})
    .post('/resource_patch', resourcePatchBody)
    .reply(200, {success: true})

  nock('http://ckan:5000/api/3/action/')
    .persist()
    .get('/package_show?name_or_id=capacityauctiondk-test')
    .reply(200, {result: descriptor})

  nock('http://ckan:5000/api/3/action/', {reqheaders: {authorization: 'wrong-api-key'}})
    .persist()
    .post('/package_create', pkgBody)
    .reply(403, {success: false, error: {message: "Access denied"}})
    .post('/custom_dataset_purge', {id: 'capacityauctiondk-test'})
    .reply(403, {success: false, error: {message: "Access denied"}})
}

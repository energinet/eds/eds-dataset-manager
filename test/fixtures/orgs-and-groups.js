const nock = require('nock')
const utils = require('../../lib')

module.exports.mocksForOrgsAndGroupsActions = function () {
  // nock.recorder.rec()

  nock('http://ckan:5000', {"encodedQueryParams":true})
    .post('/api/3/action/group_create', {"name":"my-group-name","title":"My human readable group name","description":"This is an example group.","image_url":"https://example.com/picture.png","packages":[],"id":"my-group-name"})
    .reply(200, {"help":"http://127.0.0.1:5000/api/3/action/help_show?name=group_create","success":true,"result":{"approval_status":"approved","image_display_url":"https://example.com/picture.png","package_count":0,"title":"My human readable group name","name":"my-group-name","is_organization":false,"state":"active","extras":[],"image_url":"https://example.com/picture.png","groups":[],"type":"group","users":[{"capacity":"admin","name":"test_sysadmin_00"}],"num_followers":0,"id":"my-group-name","tags":[],"description":"This is an example group."}}, [ 'Server',
    'PasteWSGIServer/0.5 Python/2.7.12',
    'Date',
    'Wed, 18 Sep 2019 08:54:42 GMT',
    'Pragma',
    'no-cache',
    'Cache-Control',
    'no-cache',
    'Content-Type',
    'application/json;charset=utf-8',
    'Content-Length',
    '575' ]);

  nock('http://ckan:5000', {"encodedQueryParams":true})
    .get('/api/3/action/group_show')
    .query({"id":"my-group-name","include_datasets":"True","include_dataset_count":"False","include_extras":"False","include_users":"False","include_groups":"False"})
    .reply(200, {"help":"http://127.0.0.1:5000/api/3/action/help_show?name=group_show","success":true,"result":{"approval_status":"approved","image_display_url":"https://example.com/picture.png","package_count":0,"title":"My human readable group name","name":"my-group-name","is_organization":false,"image_url":"https://example.com/picture.png","type":"group","packages":[],"num_followers":0,"id":"my-group-name","tags":[],"description":"This is an example group."}}, [ 'Server',
    'PasteWSGIServer/0.5 Python/2.7.12',
    'Date',
    'Wed, 18 Sep 2019 09:06:28 GMT',
    'Pragma',
    'no-cache',
    'Cache-Control',
    'no-cache',
    'Content-Type',
    'application/json;charset=utf-8',
    'Content-Length',
    '480' ]);

  nock('http://ckan:5000', {"encodedQueryParams":true})
    .post('/api/3/action/group_patch', {"name":"my-group-name","title":"Updated","id":"my-group-name"})
    .reply(200, {"help":"http://127.0.0.1:5000/api/3/action/help_show?name=group_patch","success":true,"result":{"users":[{"email_hash":"c9ef0b36942cc1f9f57267ac681f11ac","about":"Just another test sysadmin.","capacity":"admin","name":"test_sysadmin_00","created":"2019-03-27T21:26:27.281745","id":"5a6c2aa5-dece-4792-adfd-b825978f321f","sysadmin":true,"activity_streams_email_notifications":false,"state":"active","number_of_edits":213,"display_name":"Mr. Test Sysadmin","fullname":"Mr. Test Sysadmin","number_created_packages":4}],"display_name":"Updated","description":"This is an example group.","image_display_url":"https://example.com/picture.png","title":"Updated","package_count":0,"created":"2019-09-18T15:18:28.300258","approval_status":"approved","is_organization":false,"state":"active","extras":[],"image_url":"https://example.com/picture.png","groups":[],"revision_id":"60f8c6e4-cc73-4c73-a8a1-696490082342","packages":[],"type":"group","id":"my-group-name","tags":[],"name":"my-group-name"}}, [ 'Server',
    'PasteWSGIServer/0.5 Python/2.7.12',
    'Date',
    'Wed, 18 Sep 2019 09:19:13 GMT',
    'Pragma',
    'no-cache',
    'Cache-Control',
    'no-cache',
    'Content-Type',
    'application/json;charset=utf-8',
    'Content-Length',
    '1057' ]);

  nock('http://ckan:5000', {"encodedQueryParams":true})
    .post('/api/3/action/group_purge', {"name":"my-group-name","id":"my-group-name"})
    .reply(200, {"help":"http://127.0.0.1:5000/api/3/action/help_show?name=group_purge","success":true,"result":null}, [ 'Server',
    'PasteWSGIServer/0.5 Python/2.7.12',
    'Date',
    'Wed, 18 Sep 2019 11:39:13 GMT',
    'Pragma',
    'no-cache',
    'Cache-Control',
    'no-cache',
    'Content-Type',
    'application/json;charset=utf-8',
    'Content-Length',
    '106' ]);
}

import test from 'ava'
const request = require('supertest')
const mocks = require('./fixtures')
const app = require('../index').makeApp()


mocks.initMocks()

const descriptor = {
  "name": "capacityauctiondk-test",
  "description": "PTR is explicit auction of transfer capacity on a connection between two price areas",
  "title": "Auction of Capacities, PTR DK1-Germany",
  "resolution": "One hour (PT1H)",
  "update_frequency": "P1D",
  "owner_org": "test_org_00",
  "resources": [
    {
      "name": "capacityauctiondk-test",
      "path": "fake",
      "schema": {
        "fields": [
          {
            "name": "a",
            "type": "string"
          },
          {
            "name": "b",
            "type": "string"
          }
        ],
        "primaryKey": ["a"]
      }
    }
  ]
}


test('status check', async t => {
	t.plan(2)

	const res = await request(app)
		.get('/status')

	t.is(res.status, 200)
	t.is(res.text, 'server is running')
})


test('"dataset_create" with correct descriptor works', async t => {
  t.plan(2)

  const res = await request(app)
    .post('/dataset_create')
    .send(descriptor)
    .set('Authorization', 'test-api-key')

  const expected = {
    success: true,
    message: 'Dataset is created'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})

test('"dataset_create" with empty descriptor fails', async t => {
  t.plan(2)

  const res = await request(app)
    .post('/dataset_create')
    .send({})
    .set('Authorization', 'test-api-key')

  const expected = 'Error: Descriptor validation error'

  t.is(res.status, 400)
  t.true(res.text.includes(expected))
})

test('"dataset_create" with invalid field name fails', async t => {
  const copyOfDescriptor = JSON.parse(JSON.stringify(descriptor))
  copyOfDescriptor.resources[0].schema.fields[0].name += ' '
  const res = await request(app)
    .post('/dataset_create')
    .send(copyOfDescriptor)
    .set('Authorization', 'test-api-key')

  const expected = {
    success: false,
    error: {
      fields: '"a " is not a valid field name',
    }
  }

  t.is(res.status, 500)
  t.deepEqual(res.body, expected)
})


test('"dataset_create" with wrong api key fails', async t => {
  t.plan(2)

  const res = await request(app)
    .post('/dataset_create')
    .send(descriptor)
    .set('Authorization', 'wrong-api-key')

  const expected = {
    success: false,
    error: {
      message: "Access denied"
    }
  }

  t.is(res.status, 403)
  t.deepEqual(res.body, expected)
})


test('"dataset_purge" with correct body and api key works', async t => {
  t.plan(2)

  const res = await request(app)
    .post('/dataset_purge')
    .send({name: 'capacityauctiondk-test'})
    .set('Authorization', 'test-api-key')

  const expected = {
    success: true,
    message: 'Dataset is purged'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})


test('"dataset_purge" with wrong api key fails', async t => {
  t.plan(2)

  const res = await request(app)
    .post('/dataset_purge')
    .send({name: 'capacityauctiondk-test'})
    .set('Authorization', 'wrong-api-key')

  const expected = {
    success: false,
    error: {
      message: "Access denied"
    }
  }

  t.is(res.status, 403)
  t.deepEqual(res.body, expected)
})


test('"dataset_update" with correct body and api key works', async t => {
  t.plan(2)

  const body = {
    name: 'capacityauctiondk-test',
    title: 'New title',
    resources: JSON.parse(JSON.stringify(descriptor.resources))
  }
  body.resources[0].title = 'New res title'

  const res = await request(app)
    .post('/dataset_update')
    .send(body)
    .set('Authorization', 'test-api-key')

  const expected = {
    success: true,
    message: 'Dataset is updated'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})

test('"dataset_update" fails when trying to change field name or type', async t => {
  const newDescriptor = JSON.parse(JSON.stringify(descriptor))
  newDescriptor.resources[0].schema.fields[0].name = 'c'
  newDescriptor.title = 'New title'

  const agent = request(app)
  let res = await agent
    .post('/dataset_update')
    .send(newDescriptor)
    .set('Authorization', 'test-api-key')

  const expected = {
    success: false,
    error: {
      message: 'Modifying `name` and `type` properties of field is forbidden.'
    }
  }

  t.is(res.status, 400)
  t.deepEqual(res.body, expected)

  // Now test with modified 'type'
  newDescriptor.resources[0].schema.fields[0].name = 'a'
  newDescriptor.resources[0].schema.fields[0].type = 'number'
  res = await agent
    .post('/dataset_update')
    .send(newDescriptor)
    .set('Authorization', 'test-api-key')

  t.is(res.status, 400)
  t.deepEqual(res.body, expected)
})


test('"dataset_update" fails if field name or type is not provided', async t => {
  const newDescriptor = JSON.parse(JSON.stringify(descriptor))
  delete newDescriptor.resources[0].schema.fields[0].name
  newDescriptor.title = 'New title'

  const agent = request(app)
  let res = await agent
    .post('/dataset_update')
    .send(newDescriptor)
    .set('Authorization', 'test-api-key')

  const expected = {
    success: false,
    error: {
      message: 'Explicitly provide `name` and `type` properties of a field.'
    }
  }

  t.is(res.status, 400)
  t.deepEqual(res.body, expected)

  // Now test with undefined 'type'
  newDescriptor.resources[0].schema.fields[0].name = 'a'
  delete newDescriptor.resources[0].schema.fields[0].type
  res = await agent
    .post('/dataset_update')
    .send(newDescriptor)
    .set('Authorization', 'test-api-key')

  t.is(res.status, 400)
  t.deepEqual(res.body, expected)
})


test('validate with valid descriptor', async t => {
  const res = await request(app)
    .post('/validate')
    .send(descriptor)

  const expected = {
    valid: true,
    message: 'Your data package descriptor is valid'
  }
  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})


test('validate with invalid descriptor', async t => {
  const descriptor = {
    name: 'Bad Name'
  }
  const res = await request(app)
    .post('/validate')
    .send(descriptor)

  t.is(res.status, 200)
  t.is(res.body.valid, false)
  t.is(res.body.message.length, 2)
  // No 'resources' property:
  t.truthy(res.body.message.find(msg => msg.includes('Missing required property: resources')))
  // 'name' is invalid:
  t.truthy(res.body.message.find(msg => msg.includes('String does not match pattern: ^([-a-z0-9._/])+$')))
})


test('ckan to data package converter', async t => {
  const ckanDescriptor = {
    name: 'CamelCaseName',
    resources: [
      {
        resource: {
          name: 'resource'
        },
        url: 'http://example.com',
        attributes: [
          {
            "comment": "A long comment...",
            "name_of_field": "HourUTC",
            "format_regex": "",
            "property_constraint": "",
            "name_of_attribute": "Hour UTC",
            "attribute_description": "A long description",
            "id": "HourUTC",
            "validation_rules": "...",
            "type": "timestamptz",
            "example": "2017-07-14T08:00Z",
            "unit": "",
            "size": "17"
          }
        ]
      }
    ]
  }
  const res = await request(app)
    .post('/ckan-to-data-package')
    .send(ckanDescriptor)

  const expected = {
    name: 'camelcasename',
    resources: [
      {
        name: 'resource',
        path: 'http://example.com',
        schema: {
          fields: [
            {
              name: 'HourUTC',
              title: 'Hour UTC',
              description: 'A long description',
              type: 'datetime',
              format: 'any',
              comment: 'A long comment...',
              constraints: {
                pattern: ''
              },
              example: '2017-07-14T08:00Z',
              unit: ''
            }
          ]
        }
      }
    ]
  }
  t.deepEqual(res.body, expected)
})


// Groups and orgs
test('Create group/org', async t => {
  t.plan(4)
  const agent = request(app)

  const body = {
    "name": "my-group-name",
    "title": "My human readable group name",
    "description": "This is an example group.",
    "image_url": "https://example.com/picture.png",
    "packages": []
  }

  let res = await agent
    .post('/group/create')
    .send(body)
    .set('Authorization', 'some-key')

  let expected = {
    success: true,
    message: 'group create succeeded for "my-group-name".'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)

  // Compare against show response
  res = await agent
    .get('/group/show/my-group-name')

  t.is(res.body.name, body.name)
  t.is(res.body.title, body.title)
})

test('Patch group/org', async t => {
  t.plan(2)

  const body = {
    "name": "my-group-name",
    "title": "Updated"
  }

  const res = await request(app)
    .post('/group/patch')
    .send(body)
    .set('Authorization', 'some-key')

  const expected = {
    success: true,
    message: 'group patch succeeded for "my-group-name".'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})

test('Purge group/org', async t => {
  t.plan(2)

  const body = {
    "name": "my-group-name"
  }

  const res = await request(app)
    .post('/group/purge')
    .send(body)
    .set('Authorization', 'some-key')

  const expected = {
    success: true,
    message: 'group purge succeeded for "my-group-name".'
  }

  t.is(res.status, 200)
  t.deepEqual(res.body, expected)
})

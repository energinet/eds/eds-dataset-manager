FROM node:8-slim

# Create app directory
WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn

# Bundle app source
COPY . .

ENV CKAN_API http://eds-test/api/3/action/

EXPOSE 8089

CMD [ "yarn", "start" ]

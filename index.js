const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./routes')


module.exports.makeApp = function() {
  const app = express()
  app.set('port', 3000)

  // Middlewares
  app.use(bodyParser.json())
  app.use(
    bodyParser.urlencoded({
      extended: true
    })
  )

  // Controllers
  app.use([
    routes()
  ])

  return app
}


module.exports.start = function () {
  return new Promise((resolve, reject) => {
    const app = module.exports.makeApp()

    let server = app.listen(app.get('port'), () => {
      console.log('Listening on :' + app.get('port'))
      resolve(server)
    })
    app.shutdown = function () {
      server.close()
      server = null
    }
  })
}


module.exports.start()
